<!--
SPDX-FileCopyrightText: 2021 Romain Vigier <contact AT romainvigier.fr>
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# UI Shooter

Take screenshots of GTK4 widgets.


## Installing from source

UI Shooter is written in Rust using the [gtk-rs](https://gtk-rs.org/) bindings.

Install using `cargo`:

```sh
cargo install --path .
```

Runtime dependencies:

- gettext
- GTK4
- libadwaita


## Usage

Write an XML file in the [GtkBuilder format](https://docs.gtk.org/gtk4/class.Builder.html) with the widget you want bearing the id `widget` and save it as `widget.ui`:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<interface>
  <object class="GtkButton" id="widget">
    <property name="label">Hello world</property>
  </object>
</interface>
```

Run `uishooter`:

```sh
uishooter widget.ui
```

The screenshot will be saved in a file named `widget.png` next to the UI file.

### Options

- `-o`, `--output=FILE`: Path of the output PNG file
- `-r`, `--resource-file=FILE`: Path to a gresource file to load
- `-p`, `--resource-path=PATH`: Path of the resource
- `-t`, `--textdomain=STR`: Textdomain used for translation
- `-l`, `--locale=STR`: Locale used for translation
- `-i`, `--locale-dir=DIR`: Path to a directory containing compiled translations
- `-c`, `--css=FILE`: Path to a CSS file to load
- `-s`, `--scale=INT`: Integer scale factor of the output image
- `-d`, `--dark`: Use a dark variant
- `-a`, `--libadwaita`: Use libadwaita stylesheet

See [the examples](./examples/) to learn more about using these options.


## Headless

The [`scripts/headless.py` Python script](./scripts/headless.py) uses Weston to take screenshots in a headless environment, such as the one built by the [Dockerfile](./Dockerfile).

For example, using [`podman`](https://podman.io/) and the [hosted image](https://gitlab.com/rmnvgr/uishooter/container_registry):

```sh
# `/data` is the default working directory of the container
podman run --rm -v .:/data:z registry.gitlab.com/rmnvgr/uishooter widget.ui
```
