<!--
SPDX-FileCopyrightText: 2022 Romain Vigier <contact AT romainvigier.fr>
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Examples

All the needed files for the examples are provided, make sure you run `uishooter` in this directory.


## Default settings

```sh
uishooter button.ui
```

Result:

![GTK button with the text "Hello World"](./images/button.png)


## Scaling the output

```sh
uishooter --scale=2 button.ui
```

Result:

![GTK button with the text "Hello World" at double scale](./images/scaled.png)


## Using the libadwaita stylesheet

```sh
uishooter --libadwaita button.ui
```

Result:

![GTK button with the text "Hello World" in the libadwaita style](./images/libadwaita.png)


## Using the dark color scheme

```sh
uishooter --dark button.ui
```

Result:

![Dark GTK button with the text "Hello World"](./images/dark.png)

Combining it with libadwaita:

```sh
uishooter --libadwaita --dark button.ui
```

Result:

![Dark GTK button with the text "Hello World" in the libadwaita style](./images/libadwaita-dark.png)


## Loading a custom CSS file

```sh
uishooter --css=style.css button.ui
```

Result:

![GTK button with the text "Hello World" and a yellow border](./images/custom-css.png)


## Loading a translation

Compile the [translation](./translation.po):

```sh
mkdir -p ./locale/fr/LC_MESSAGES
msgfmt --output-file=./locale/fr/LC_MESSAGES/example.mo translation.po
```

```sh
uishooter \
	--textdomain=example \
	--locale=fr_FR \
	--locale-dir=locale \
	button.ui
```

Result:

![GTK button with the text "Hello World" translated in French](./images/translation.png)


## Loading custom resources

Compile the [resources](./resources.gresource.xml):

```sh
glib-compile-resources resources.gresource.xml
```

```sh
uishooter \
	--resource-file=resources.gresource \
	--resource-path=/org/example/app \
	image-button.ui
```

Result:

![GTK button with the UI Shooter's logo and the text "Hello World"](./images/resources.png)


## Everything

Of course, it is possible to use all the options at the same time:

```sh
uishooter \
	--dark \
	--libadwaita \
	--scale=2 \
	--css=style.css \
	--textdomain=example \
	--locale=fr_FR \
	--locale-dir=locale \
	--resource-file=resources.gresource \
	--resource-path=/org/example/app \
	image-button.ui
```

Result:

![GTK button with the UI Shooter's logo and the text "Hello World" translated in French, with a yellow border and at double scale](./images/everything.png)
