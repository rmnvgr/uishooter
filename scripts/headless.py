#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2021 Romain Vigier <contact AT romainvigier.fr>
# SPDX-License-Identifier: GPL-3.0-or-later

"""Run UI Shooter in a headless compositor."""

import os
import subprocess
import sys

from typing import List


def start_weston(scale=1) -> subprocess.Popen[bytes]:
    """Start the Weston compositor in headless mode.

    Args:
        scale (int, optional): Output scale. Defaults to 1.

    Returns:
        subprocess.Popen[bytes]: The opened Weston subprocess.
    """
    return subprocess.Popen(
        [
            "weston",
            "--backend=headless-backend.so",
            f"--scale={scale}",
            "--socket=wayland-100"],
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL)


def run_uishooter(args: List[str]) -> int:
    """Run UI Shooter.

    Args:
        args (List[str]): Arguments passed to uishooter.

    Returns:
        int: Exit code.
    """
    env = os.environ.copy()
    env["WAYLAND_DISPLAY"] = "wayland-100"
    return subprocess.run(["uishooter"] + args, env=env).returncode


if __name__ == "__main__":
    weston = None
    exit_code = 0
    scale = 1

    # Grab the scale from the arguments
    for i, arg in enumerate(sys.argv):
        if arg == "-s" or arg == "--scale":
            scale = int(sys.argv[i + 1])
        elif arg[:8] == "--scale=":
            scale = int(arg.split("=")[1])

    try:
        weston = start_weston(scale)
        exit_code = run_uishooter(sys.argv[1:])
    finally:
        if weston:
            weston.terminate()

    sys.exit(exit_code)
