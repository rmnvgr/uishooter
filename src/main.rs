/*
SPDX-FileCopyrightText: 2021 Romain Vigier <contact AT romainvigier.fr>
SPDX-License-Identifier: GPL-3.0-or-later
*/

use std::process;
use std::path::Path;

use gtk::prelude::*;
use gtk::{gdk, gio, glib, gsk};


fn main() {
    gtk::init().unwrap();

    // Create a paintable that will hold the user's widget
    let paintable = gtk::WidgetPaintable::new(None::<&gtk::Widget>);

    // Create a new application
    let app = gtk::Application::builder()
        .application_id("fr.romainvigier.uishooter")
        .flags(gio::ApplicationFlags::HANDLES_COMMAND_LINE)
        .build();

    app.set_option_context_parameter_string(Some("FILE"));
    app.set_option_context_summary(Some(
        "Take a screenshot of the object bearing the `widget` id in the given \
        FILE, written in GtkBuilder format, and output a PNG file."));

    app.add_main_option(
        "output",
        glib::Char::from(b'o'),
        glib::OptionFlags::NONE,
        glib::OptionArg::Filename,
        "Path of the output PNG file",
        Some("FILE"),
    );

    app.add_main_option(
        "resource-file",
        glib::Char::from(b'r'),
        glib::OptionFlags::NONE,
        glib::OptionArg::Filename,
        "Path to a gresource file to load",
        Some("FILE"),
    );

    app.add_main_option(
        "resource-path",
        glib::Char::from(b'p'),
        glib::OptionFlags::NONE,
        glib::OptionArg::String,
        "Path of the resource",
        Some("PATH"),
    );

    app.add_main_option(
        "textdomain",
        glib::Char::from(b't'),
        glib::OptionFlags::NONE,
        glib::OptionArg::String,
        "Textdomain used for translation",
        Some("STR"),
    );

    app.add_main_option(
        "locale",
        glib::Char::from(b'l'),
        glib::OptionFlags::NONE,
        glib::OptionArg::String,
        "Locale used for translation",
        Some("STR"),
    );

    app.add_main_option(
        "locale-dir",
        glib::Char::from(b'i'),
        glib::OptionFlags::NONE,
        glib::OptionArg::Filename,
        "Path to a directory containing compiled translations",
        Some("DIR"),
    );

    app.add_main_option(
        "css",
        glib::Char::from(b'c'),
        glib::OptionFlags::NONE,
        glib::OptionArg::Filename,
        "Path to a CSS file to load",
        Some("FILE"),
    );

    app.add_main_option(
        "scale",
        glib::Char::from(b's'),
        glib::OptionFlags::NONE,
        glib::OptionArg::Int,
        "Integer scale factor of the output image",
        Some("INT"),
    );

    app.add_main_option(
        "dark",
        glib::Char::from(b'd'),
        glib::OptionFlags::NONE,
        glib::OptionArg::None,
        "Use a dark variant",
        None,
    );

    app.add_main_option(
        "libadwaita",
        glib::Char::from(b'a'),
        glib::OptionFlags::NONE,
        glib::OptionArg::None,
        "Use libadwaita stylesheet",
        None,
    );

    app.connect_command_line(move |app, command_line| {
        let args = command_line.arguments();
        if args.len() < 2 {
            glib::g_printerr!("No UI file given. Run with `--help` to learn how to use it.");
            return 1;
        }
        let input_file = command_line.create_file_for_arg(&command_line.arguments()[1]);

        let display: gdk::Display;
        match gdk::Display::default() {
            Some(d) => display = d,
            None => {
                glib::g_printerr!("Unable to get a display.");
                process::exit(1);
            }
        };

        let options = command_line.options_dict();

        // Set output file
        let output: gio::File;
        match options.lookup_value("output", None) {
            Some(variant) => output = parse_filename_variant(variant),
            _ => output = gio::File::for_path(input_file.path().unwrap().with_extension("png")),
        };

        // Create parent directory
        output
            .parent()
            .unwrap_or_else(|| {
                glib::g_printerr!("Unable to get the parent directory of the output file.");
                process::exit(1);
            })
            .make_directory_with_parents(None::<&gio::Cancellable>)
            .unwrap_or_else(|err| {
                if let Some(io_error) = err.kind::<gio::IOErrorEnum>() {
                    match io_error {
                        gio::IOErrorEnum::Exists => (),
                        _ => {
                            glib::g_printerr!("{}", err);
                            process::exit(1);
                        }
                    }
                }
            });

        // Load resource
        match options.lookup_value("resource-file", None) {
            Some(variant) => {
                let path = parse_filename_variant(variant).path().unwrap();
                match gio::Resource::load(path) {
                    Ok(resource) => gio::resources_register(&resource),
                    Err(err) => {
                        glib::g_printerr!("{}", err.message());
                        process::exit(1);
                    }
                };
            },
            _ => (),
        };
        match options.lookup_value("resource-path", None) {
            Some(path) => {
                let icon_theme = gtk::IconTheme::for_display(&display);
                icon_theme.add_resource_path(Path::new(path.str().unwrap()).join("icons").to_str().unwrap());
            },
            _ => (),
        };

        // Get locale
        let locale: String;
        match options.lookup_value("locale", None) {
            Some(variant) => locale = variant.str().unwrap().to_string(),
            _ => locale = "C".to_string(),
        };

        let locale_dir = options.lookup_value("locale-dir", None).map(|variant| {
            parse_filename_variant(variant)
                .parent()
                .unwrap_or_else(|| {
                    glib::g_printerr!("Unable to get the parent directory of the locale directory.");
                    process::exit(1);
                })
                .path().unwrap()
        });

        // Load translations
        match options.lookup_value("textdomain", None) {
            Some(domain) => gettext::TextDomain::new(domain.str().unwrap())
                .locale(&locale)
                .locale_category(gettext::LocaleCategory::LcAll)
                .prepend(locale_dir.unwrap_or_default())
                .init().unwrap_or_else(|err| {
                    glib::g_printerr!("{}", err);
                    process::exit(1);
                }),
            _ => gettext::setlocale(gettext::LocaleCategory::LcAll, locale),
        };

        // Set text direction
        let text_direction = gtk::locale_direction();
        gtk::Widget::set_default_direction(text_direction);

        // Load user CSS
        match options.lookup_value("css", None) {
            Some(variant) => {
                let file = parse_filename_variant(variant);
                let provider = gtk::CssProvider::new();
                provider.load_from_file(&file);
                gtk::StyleContext::add_provider_for_display(
                    &display,
                    &provider,
                    gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
                );
            },
            _ => (),
        };

        // Get scale
        let scale: i32;
        match options.lookup_value("scale", None) {
            Some(variant) => scale = variant.get().unwrap(),
            _ => scale = 1,
        };

        // Initialize libadwaita if requested
        let libadwaita: bool;
        match options.lookup_value("libadwaita", None) {
            Some(_) => {
                adw::init().unwrap();
                libadwaita = true;
            },
            _ => libadwaita = false,
        };

        // Set dark theme
        match options.lookup_value("dark", None) {
            Some(_) => {
                if libadwaita {
                    adw::StyleManager::default().set_color_scheme(adw::ColorScheme::ForceDark);
                }
                else {
                    gtk::Settings::default().unwrap().set_property("gtk-application-prefer-dark-theme", true);
                }
            },
            _ => {
                if libadwaita {
                    adw::StyleManager::default().set_color_scheme(adw::ColorScheme::ForceLight);
                }
                else {
                    gtk::Settings::default().unwrap().set_property("gtk-application-prefer-dark-theme", false);
                }
            },
        };

        // Build user widget
        let builder = gtk::Builder::from_file(input_file.path().unwrap());
        let widget: gtk::Widget = builder.object("widget").unwrap_or_else(|| {
            glib::g_printerr!("No object with the id `widget` found in the UI file.");
            process::exit(1);
        });

        // Set up the paintable to render and save the widget when displayed
        paintable.set_widget(Some(&widget));
        paintable.connect_invalidate_contents(move |paintable| {
            match paintable.widget() {
                Some(widget) => {
                    let snapshot = gtk::Snapshot::new();
                    paintable.snapshot(
                        &snapshot,
                        (widget.allocated_width() * scale).into(),
                        (widget.allocated_height() * scale).into()
                    );
                    let mut node = snapshot.to_node().unwrap();
                    if node.node_type() == gsk::RenderNodeType::ClipNode {
                        node = node.downcast::<gsk::ClipNode>().unwrap().child();
                    }
                    let renderer = widget.native().unwrap().renderer();
                    let texture = renderer.render_texture(&node, None);
                    texture.save_to_png(output.path().unwrap()).unwrap_or_else(|err| {
                        glib::g_printerr!("{}", err);
                        process::exit(1);
                    });
                    gio::Application::default().unwrap().quit();
                },
                _ => (),
            };
        });

        // Display widget
        let window: gtk::Window;
        if widget.is::<gtk::Window>() {
            window = widget.downcast::<gtk::Window>().unwrap();
            window.set_application(Some(app));
        }
        else if widget.is::<gtk::Popover>() {
            let popover = widget.downcast::<gtk::Popover>().unwrap();
            popover.set_autohide(false);
            window = gtk::Window::builder()
                .application(app)
                .child(&popover)
                .build();
            popover.popup();
        }
        else {
            widget.set_halign(gtk::Align::Start);
            widget.set_valign(gtk::Align::Start);
            window = gtk::Window::builder()
                .application(app)
                .child(&widget)
                .build();
        }
        window.present();
        0
    });

    // Run the application
    process::exit(app.run());
}

fn parse_filename_variant(variant: glib::Variant) -> gio::File {
    let mut path = variant.get::<Vec<u8>>().unwrap();
    path.pop().unwrap();
    let path = String::from_utf8(path).unwrap();
    gio::File::for_path(path)
}
