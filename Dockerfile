# SPDX-FileCopyrightText: 2021 Romain Vigier <contact AT romainvigier.fr>
# SPDX-License-Identifier: GPL-3.0-or-later

FROM fedora:37 as build

RUN dnf install --setopt=install_weak_deps=False --nodocs -y \
		cargo git rust \
		gettext-devel gtk4-devel libadwaita-devel

WORKDIR /buildroot

# Build UI Shooter
WORKDIR /src/uishooter
COPY ./src ./src
COPY ./Cargo.toml ./
RUN cargo install --path . --root /buildroot/usr

# Cleanup
RUN rm -rf /buildroot/usr/.crates*


FROM fedora:37

RUN echo "%_install_langs all" > /etc/rpm/macros.image-language-conf \
	&& dnf install --setopt=install_weak_deps=False --nodocs -y \
		abattis-cantarell-fonts gettext glib2-devel glibc-all-langpacks \
		gtk4 langpacks-core-font-* libadwaita python-unversioned-command \
		python3-pip weston \
	&& dnf clean all

# Create XDG_RUNTIME_DIR for Weston
RUN mkdir -p /run/user/root \
	&& chown root:root /run/user/root \
	&& chmod 700 /run/user/root
ENV XDG_RUNTIME_DIR=/run/user/root

# Add UI Shooter
COPY --from=build /buildroot /

# Add headless script
COPY ./scripts/headless.py /usr/bin/uishooter-headless
RUN chmod a+x /usr/bin/uishooter-headless

WORKDIR /data

ENTRYPOINT [ "uishooter-headless" ]
